cmake_minimum_required(VERSION 3.5)

set(CMAKE_CXX_FLAGS "-std=c++1z")

add_library(Lexer-for-compiler-design-Trandon-Ware
file.cpp
decl.cpp
stmt.cpp
ast.cpp
debug.cpp
codegen.cpp
location.cpp
symbol.cpp
token.cpp
lexer.cpp)

target_link_libraries(lang ${LLVM_LIBS})

add_executable(mc main.cpp)
target_link_libraries(mc lang)
