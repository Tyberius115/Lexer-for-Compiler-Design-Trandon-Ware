#include "decl.hpp"
#include "type.hpp"
#include "debug.hpp"

#include <iostream>

void
decl::debug() const
{
  debug_printer dp(std::cerr);
  ::debug(dp, this);
}

bool
object_decl::is_reference() const
{
  return get_type()->is_reference();
}

fn_type* 
fn_decl::get_type() const
{
  return static_cast<fn_type*>(m_type);
}

type* 
fn_decl::get_return_type() const
{
  return get_type()->get_return_type();
}
