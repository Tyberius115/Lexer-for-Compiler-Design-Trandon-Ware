#pragma once

#include "symbol.hpp"

#include <vector>

class type;
class fn_type;
class expr;
class stmt;

/// Represents the set of all expressions.
class decl
{
public:
  enum kind {
    prog_kind,
    var_kind,
    const_kind,
    value_kind,
    parm_kind,
    fn_kind,
  };

protected:
  decl(kind k, symbol sym)
    : m_kind(k), m_name(sym)
  { }

public:
  virtual ~decl() = default;

  kind get_kind() const { return m_kind; }

  /// Returns true if this is a program.
  bool is_program() const { return m_kind == prog_kind; }
  
  /// Returns true if this is a variable.
  bool is_variable() const { return m_kind == var_kind || m_kind == parm_kind; }

  symbol get_name() const { return m_name; }

  void debug() const;

private:
  kind m_kind;
  symbol m_name;
};


using decl_list = std::vector<decl*>;


// Represents the entire program.
struct prog_decl : decl
{
  prog_decl(const decl_list& ds)
    : decl(prog_kind, nullptr), m_decls(ds)
  { }

  const decl_list& get_declarations() const { return m_decls; }
  decl_list& get_declarations() { return m_decls; }

  decl_list m_decls;
};

/// Any declaration described by a type.
struct typed_decl : decl
{
protected:
  typed_decl(kind k, symbol sym, type* t)
    : decl(k, sym), m_type(t)
  { }

public:
  type* get_type() const { return m_type; }

  void set_type(type* t) { m_type = t; }

protected:
  type* m_type;
};


// The base class of variables, constants, and values.
struct object_decl : typed_decl
{
protected:
  object_decl(kind k, symbol sym, type* t, expr* e)
    : typed_decl(k, sym, t), m_init(e)
  { }

public:
  /// Returns true if the declaration has reference type.
  bool is_reference() const;

  /// Returns the initializer.  
  expr* get_init() const { return m_init; }

  /// Sets the initializer.
  void set_init(expr* e) { m_init = e; }

protected:
  expr* m_init;
};


/// Represents the declaration of a variable.
struct var_decl : object_decl
{
  var_decl(symbol sym, type* t, expr* e = nullptr)
    : object_decl(var_kind, sym, t, e)
  { }
};


/// Represents the declaration of a constant.
struct const_decl : object_decl
{
  const_decl(symbol sym, type* t, expr* e = nullptr)
    : object_decl(const_kind, sym, t, e)
  { }
};


/// Represents the declaration of a value.
struct value_decl : object_decl
{
  value_decl(symbol sym, type* t, expr* e = nullptr)
    : object_decl(value_kind, sym, t, e)
  { }
};


/// Represents the declaration of a parameter of a function.
struct parm_decl : object_decl
{
  parm_decl(symbol sym, type* t)
    : object_decl(parm_kind, sym, t, nullptr)
  { }
};


/// Represents a function definition.
struct fn_decl : typed_decl
{
  fn_decl(symbol sym, type* t, const decl_list& parms, stmt* s = nullptr)
    : typed_decl(fn_kind, sym, t), m_parms(parms), m_body(s)
  { }

  const decl_list& get_parameters() const { return m_parms; }
  decl_list& get_parameters() { return m_parms; }

  fn_type* get_type() const;

  type* get_return_type() const;

  stmt* get_body() const { return m_body; }
  void set_body(stmt* s) { m_body = s; }

  decl_list m_parms;
  stmt* m_body;
};
