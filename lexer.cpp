#include "lexer.hpp"
#include "file.hpp"

#include <cassert>
#include <cctype>
#include <sstream>
#include <iostream>

//character classes
static bool
is_space(char c)
{
	return c == ' ' || c == '\t';
}

static bool
is_newline(char c)
{
	return c == '\n';
}

static bool
is_nondigit(char c)
{
	return std::isalpha(c) || c == '_';
}

static bool
is_digit(char c)
{
	return std::isdigit(c);
}

static bool
is_alphanumeric(char c)
{
	return std::isalphanumeric(c)
}

static bool
is_bin_digit(char c)
{
	return c == '0' || c == '1';
}

static bool
is_oct_digit(char c)
{
	return c == '0' || c == '1' || c == '2'
	|| c == '3' || c == '4' || c == '5' || c == '6'
	|| c == '7';
}

static bool
is_hex_digit(char c)
{
	return std::isxdigit(c);
}

static const char*
get_start_of_input(const file& f)
{
	return f.get_text().data();
}

static const char*
get_end_of_input(const file& f)
{
	return f.get_text().data() + f.get_text().size();
}

lexer::lexer(symbol_table& syms, const file& f)
	: m_syms(syms),
	 m_first(get_start_of_input(f)),
	 m_last(get_end_of_input(f)),
	 m_current_locn(f, 0, 0)
{
	m_reserved.insert({
		{m_syms.get("and"), kw_and},
		{m_syms.get("as"), kw_as},
		{m_syms.get("bool"), kw_bool},
		{m_syms.get("def"), kw_def}, //keywords
		{m_syms.get("else"), kw_else},
		{m_syms.get("false"), kw_false},
		{m_syms.get("float"), kw_float},
		{m_syms.get("if"), kw_if},
		{m_syms.get("int"), kw_int},
		{m_syms.get("let"), kw_let},
		{m_syms.get("not"), kw_not},
		{m_syms.get("or"), kw_or},
		{m_syms.get("return"), kw_return},
		{m_syms.get("true"), kw_true},
		{m_syms.get("var"), kw_var},
		{m_syms.get("void"), kw_void},
		{m_syms.get("while"), kw_while},
		
		{m_syms.get("and"), logical_and}, //logicals
		{m_syms.get("or"), logical_or},
		{m_syms.get("not"), logical_not},
		
		{m_syms.get("true"), true}, //literals
		{m_syms.get("false"), false},
		
		{m_syms.get("bool"), type_s_bool}, //type specifiers
		{m_syms.get("char"), type_s_char},
		{m_syms.get("int"), type_s_int},
		{m_syms.get("float"), type_s_float},
	});
}

//This will return true if past eof
bool
lexer::eof() const
{
	return m_first == m_last;
}

//returns current character or returns 0 if past eof
char 
lexer::peek() const
{
	return eof() ? 0: *m_first;
}

//returns the nth character past the current character
//or returns 0 if past eof
char 
lexer::peek(int n) const
{
	if (n < m_last - m_first)
		return *(m_first + n);
	else
		return 0;
}

//Accepts and returns the current character
char
lexer::accept()
{
	assert(*m_first != '\n');
	char c = *m_first;
	++m_first;
	++m_current_locn.column;
	return c;
}

void
lexer::accept(int n)
{
	while(n)
	{
		accept();
		--n;
	}
}

char
lexer::ignore()
{
	assert(*m_first != '\n');
	char c = *m_first;
	++m_first;
	++m_current_locn.column;
	return c;
}

token
lexer::scan()
{
	while (!eof())
	{
		m_token_locn = m_current_locn;
		switch (*m_first)
		{
			case ' ':
			case '\t':
				skip_space();
				continue;
				
			case '\n':
				skip_newline();
				continue;
				
			case '#':
				skip_comment();
				continue;
				
			case '(':
				return lex_punctuator(token_left_paren);
				
			case ')':
				return lex_punctuator(token_right_paren);
				
			case '[':
				return lex_punctuator(token_left_bracket);
				
			case ']':
				return lex_punctuator(token_right_bracket);
				
			case '{':
				return lex_punctuator(token_left_curly_bracket);
				
			case '}':
				return lex_punctuator(token_right_curly_bracket);
				
			case ',':
				return lex_punctuator(token_comma);
				
			case ';':
				return lex_punctuator(token_semicolon);
			
			case ':':
				return lex_punctuator(token_colon);
				
			case '<':
				if (peek(1) == '=')
					return lex_relational_operator(2, op_loe);
				if (peek(1) == '<')
					return lex_bitwise_operator(2, op_shift_left);
				return lex_relational_operator(1, op_lt);
				
			case '>':
				if (peek(1) == '=')
					return lex_relational_operator(2, op_goe);
				if (peek(1) == '>')
					return lex_bitwise_operator(2, op_shift_right);
				return lex_relational_operator(1, op_gt);
				
			case '=':
				if (peek(1) == '=')
					return lex_relational_operator(2, op_eq);
				else
					return lex_assignment_operator();
				
			case '+':
				return lex_arithmetic_operator(op_plus);
				
			case '-'
				return lex_arithmetic_operator(op_minus);
				
			case '*'
				return lex_arithmetic_operator(op_multiply);
				
			case '/'
				return lex_arithmetic_operator(op_divide);
				
			case '%'
				return lex_arithmetic_operator(op_remainder);
				
			case '&'
				return lex_arithmetic_operator(op_and);
				
			case '|'
				return lex_arithmetic_operator(op_or);
				
			case '^'
				return lex_arithmetic_operator(op_xor);
				
			case '~'
				return lex_arithmetic_operator(op_not);
				
			case '?'
				return lex_conditional_operator();
				
			case '\'':
				return lex_char();
				
			case '"':
				return lex_str();
				
			default:
			{
				if (is_nondigit(*m_first))
					return lex_word();
				if (is_digit(*m_first))
					return lex_number();
				
				std::stringstream ss;
				ss << "invalid character '" << *m_first << '\'';
				throw std::runtime_error(ss.str());
			}
		
		}
	}
	return{};
}

void
lexer::skip_space()
{
	assert(is_space(*m_first));
	ignore();
	while (!eof() && is_space(*m_first))
		ignore();
}

void
lexer::skip_newline()
{
	assert(*m_first == '\n');
	m_current_locn.line += 1;
	m_current_locn.column = 0;
	++m_first;
}

void
lexer::skip_comment()
{
	assert(*m_first == '#');
	ignore();
	while (!eof() && !is_newline(*m_first))
		ignore();
}

token
lexer::lex_punctuator(token_name n)
{
	accept();
	return {n, m_token_locn};
}

token
lexer::lex_relational_operator(int len, relational_ops op)
{
	accept(len);
	return {op, m_token_locn};
}

token
lexer::lex_arithmetic_operator(arithmetic_ops op)
{
	accept();
	return {op, m_token_locn};
}

token
lexer::lex_bitwise_operator(int len, bitwise_ops op)
{
	accept(len);
	return{op, m_token_locn};
}

token
lexer::lex_assignment_operator()
{
	accept();
	return{token_assignment_operator, m_token_locn};
}

token
lexer::lex_arrow_operator()
{
	accept(2);
	return {token_arrow_operator, m_token_locn};
}

token
lexer::lex_conditional_operator()
{
	accept();
	return {token_conditional_operator, m_token_locn};
}

token
lexer::lex_word()
{
	assert(is_nondigit(*m_first));
	const char* start = m_first;
	
	accept();
	while (!eof() && is_alphanumeric(*m_first))
		accept();
	
	std::string str(start, m_first);
	symbol sym = m_syms.get(str);
	auto iter = m_reserved.find(sym);
	if (iter != m_reserved.end()) {
		const token& token =  iter->second;
		return {token.get_name(), token.get_attribute(), m_token_locn};
	}
	else
		return {sym, m_token_locn}; //identifier
}

token
lexer::lex_number()
{
	assert(isdigit(*m_first));
	const char* start = m_first;
	
	//prefix determines the base of counting
	if (*m_first == '0') {
		char pre = peek(1);
		switch (pre) {
			case 'b': case 'B':
				return lex_bin_number();
			case 'o': case 'O':
				return lex_oct_number();
			case 'x': case 'X':
				return lex_hex_number();
			default:
				break;
		}
	}
	
	//decimal whole numbers
	accept();
	while (!eof() && is_digit(*m_first))
		accept();
	
	if (peek() != '.')
	{
		std::string str(start, m_first);
		return {decimal, std::atoi(str.c_str()), m_token_locn};
	}
	
	//fractional component
	accept();
	while (!eof() && is_digit(*m_first))
		accept();
	
	std::string str(start, m_first);
	return {std::atof(str.c_str()), m_token_locn};
}

token
lexer::lex_bin_number()
{
	accept(2);
	
	const char* start = m_first;
	while (!eof() && is_bin_digit(*m_first))
		accept();
	
	std::string str(start, m_first);
	return {bin, std::strtoll(str.c_str(), nullptr, 2), m_token_locn};
}

token
lexer::lex_oct_number()
{
	accept(2);
	
	const char* start = m_first;
	while (!eof() && is_oct_digit(*m_first))
		accept();
	
	std::string str(start, m_first);
	return {oct, std::strtoll(str.c_str(), nullptr, 8), m_token_locn};
}

token
lexer::lex_hex_number()
{
	accept(2);
	
	const char* start = m_first;
	while (!eof() && is_hex_digit(*m_first))
		accept();
	
	std::string str(start, m_first);
	return {hex, std::strtoll(str.c_str(), nullptr, 16), m_token_locn};
}

static bool
is_character_character(char c)
{
	return c != '\n' && c != '\'';
}

char
lexer::scan_escape_sequence()
{
	assert(*m_first == '\\');
	accept();
	if (eof())
		throw std::runtime_error("unterminated escape-sequence")
	switch(accept()) 
	{
		case '\'': return "\\\'"; //single quote
		case '\"': return "\\\""; //inverted commas
		case '\?': return "\\?"; //question mark
		case '\\': return "\\\\"; //backslash
		case '\a': return "\\a"; //alert
		case '\b': return "\\b"; //backspace
		case '\f': return "\\f"; //form feed
		case '\n': return "\\n"; //new line
		case '\r': return "\\r"; //return to start of line
		case '\t': return "\\t"; //tab
		case '\v': return "\\v"; //vertical tab
		default:
		throw std::runtime_error("invalid escape-sequence");
	}
}

token
lexer::lex_character()
{
	assert(*m_first == '\'');
	accept();
	
	if(eof())
		throw std::runtime_error("unterminated character literal");
	
	char c;
	if(*m_first == '\\')
		c = scan_escape_sequence();
	else if (is_character_character(*m_first))
		c = accept();
	else if (*m_first == '\'')
		throw std::runtime_error("invalid character literal")
	else if (*m_first == '\n')
		throw std::runtime_error("invalid multi-line character")
	else
		throw std::logic_error("unexpected character");
	
	if(*m_first != '\'')
		throw std::runtime_error("invalid multi-byte character");
	accept();
	
	return {c, m_token_locn};
}

static bool
is_string_character(char c)
{
	return c != '\n' && c != '"';
}

token
lexer::lex_string()
{
	assert(*m_first == '"');
	accept();
	
	if(eof())
		throw std::runtime_error("unterminated character literal")
	
	std::string str;
	str.reserve(32);
	while (*m_first != '"')
	{
		char c;
		if(*m_first == '\\')
			c = scan_escape_sequence();
		else if (is_string_character(*m_first))
			c = accept();
		else if (*M_first == '\n')
			throw std::runtime_error("invalid multi-line string");
		str += c;
	}
	accept();
	
	return {string_attr{m_syms.get(str)}, m_token_locn};
}

