#pragma once

#include "token.hpp"

#include <unordered_map>

class file;

//This lexer will transform input characters into output tokens.
//Whitespace is excluded form the output.

class lexer
{
public:
	lexer(symbol_table& syms, const file& f);
	
	token operator()()
	{
		return scan();
	}
	
	token scan();
	
	bool eof() const;
	
	char peek() const;
	char peek(int n) const;
	
private:
	char accept();
	void accept(int n);
	char ignore();
	
	//skip functions
	void skip_space();
	void skip_newline();
	void skip_comment();
	
	//accept functions (yield tokens)
	token lex_puctuator(token_name n);
	token lex_relational_operator(int len, relational_ops op);
	token lex_arithmetic_operator(arithmetic_ops op);
	token lex_bitwise_operator(int len, bitwise_ops op);
	token lex_conditional_operator();
	token lex_assignment_operator();
	token lex_arrow_operator();
	token lex_word();
	token lex_number();
	token lex_bin_number();
	token lex_oct_number();
	token lex_hex_number();
	token lex_char();
	token lex_str();
	
	char scan_escape_sequence();
	
private:
	//creates symbols
	symbol_table& m_syms;
	
	//lex state
	const char* m_first;
	const char* m_last;
	
	//current location
	location m_current_locn;
	
	//current token location
	location m_token_locn;
	
	std::unordered_map<symbol, token> m_reserved;
};