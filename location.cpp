#include "location.hpp"
#include "file.hpp"

#include <iostream>

std::ostream&
operator<<(std::ostream& os, location locn)
{
	if (locn.source)
		os << locn.source->get_path();
	else
		os << "<input>";
	os << ':' << locn.line + 1 << ':' << locn.column + 1;
	return os;
}