#include "parser.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>

inline token_name
parser::lookahead()
{
	assert(!m_tok.empty());
	return m_tok.front().get_name();
}

inline token_name
parser::lookahead(int n)
{
	if (n < m_tok.size())
		return m_tok[n].get_name();
	n = n - m_tok.size() + 1;
	while (n != 0)
	{
		fetch();
		--n;
	}
	return m_tok.back().get_name();
}

token
parser::match(token_name n)
{
	if (lookahead() == n)
		return accept();
	
	std::stringstream ss;
	ss << peek().get_location() << ": syntax error";
	throw std::runtime_error(ss.str());
}

token
parser::match_if(token_name n)
{
	if (lookahead() == n)
		return accept();
	else
		return{};
}

token
parser::match_if_logical_or()
{
	if (lookahead() == token_logical_operator)
		if(peek().get_logical_operator() == logical_and)
			return accept();
		return{};
}

token
parser::match_if_logical_and()
{
	if(lookahead() == token_logical_operator)
		if(peek().get_logical_operator() == logical_and)
			return accept();
		return {};
}

token
parser::match_if_bitwise_or()
{
	if(lookahead() == token_bitwise_operator)
		if(peek().get_bitwise_operator() == op_xor)
			return accept();
		return{};
}

token
parser::match_if_bitwise_xor()
{
	if(lookahead() == token_bitwise_operator)
		if (peek().get_bitwise_operator() == op_and)
			return accept();
		return{};
}

token
parser::match_if_equality()
{
	if (lookahead() == token_relational_operator)
	{
		switch (peek().get_relational_operator())
		{
			default:
				return {};
			case op_eq:
			case op_ne:
			return accept();
		}
	}
	return {};
}

token
parser::match_if_relational()
{
	if (lookahead() == token_relational_operator) {
		switch (peek().get_bitwise_operator()){
		default:
			return {};
		case op_lt:
		case op_gt:
		case op_loe:
		case op_goe:
			return accept();
		}
	}
	return {};
}

token
parser::match_if_shift()
{
	if (lookahead() == token_bitwise_operator) {
		switch peek().get_bitwise_operator()) {
		default:
			return{};
		case op_shift_left:
		case op_shift_right:
			return accept();
		}
	}
	return {};
}

token
parser::match_if_multiplicative()
{
	if (lookahead() == token_arithmetic_operator)
	{
		switch (peek().get_arithmetic_operator)
		{
			default:
				return{};
			case op_multiply:
			case op_divide:
			case op_remainder:
				return accept();
		}
	}
	return {};
}

token
parser::match_if_additive()
{
	if (lookahead() == token_arithmetic_operator)
	{
		switch (peek().get_arithmetic_operator)
		{
			default:
				return{};
			case op_plus:
			case op_minus:
				return accept();
		}
	}
	return {};
}

token
parser::accept()
{
	token token = peek();
	m_tok.pop_front();
	if (m_tok.empty())
		fetch();
	return tok;
}

token
parser::peek()
{
	assert(!m_tok.empty());
	return m_tok.front();
}

void
parser::fetch()
{
	m_tok.push_back(m_lex());
}

type*
parser::parse_type()
{
	return parse_basic_type();
}

//basic type -> void/bool/int/float/char/(type)
type*
parser::parse_basic_type()
{
	switch (lookahead())
	{
		case token_type_specifier:
		return m_act.on_basic_type(accept());
		
		case token_left_paren:
		{
			match(token_left_paren);
			type* t = parse_type();
			match(token_right_paren);
			return t;
			
		}
		default:
		throw std::runtime_error("expected basic-type");
	}
}

//expressions

expr*
parser::parse_expr()
{
	return parse_assignment_expr();
}

expr*
parser::parse_assignment_expr()
{
	expr* e1 = parse_conditional_expr();
	if(match_if(token_assignment_operator)){
		expr* e2 = parse_assignment_expr();
		return m_act.on_assignment_expr(e1,e2);
	}
	return e1;
}


expr*
parser::parse_conditional_expr()
{
	expr* e1 = parse_logical_or_expr();
	if(match_if(token_conditional_operator)){
		expr* e2 = parse_expr();
		match(token_colon);
		expr* e3 = parse_conditional_expr();
		return m_act.on_conditional_expr(e1,e2,e3);
	}
	return e1;
}

expr*
parser::parse_logical_or_expr()
{
	expr* e1 = parse_logical_and_expr();
	while(match_if_logical_or()){
		expr* e2 = parse_bitwise_or_expr();
		e1 = m_act.on_logical_and_expr(e1,e2);
	}
	return e1;
}

expr*
parser::parse_logical_and_expr()
{
	expr* e1 = parse_bitwise_or_expr();
	while (match_if_logical_and()) {
		expr* e2 = parse_bitwise_or_expr();
		e1 = m_act.on_logical_and_expr(e1,e2);
	}
	return e1;
}

expr*
parser::parse_bitwise_or_expr()
{
	expr* e1 = parse_bitwise_xor_expr();
	while (match_if_bitwise_or()) {
		expr* e2 = parse_bitwise_xor_expression();
		e1 = m_act.on_bitwise_or_expr(e1,e2);
	}
	return e1;
}



expr*
parser::parse_bitwise_xor_expr()
{
	expr* e1 = parse_bitwise_xor_expr();
	while(match_if_bitwise_or()){
		expr* e2 = parse_bitwise_xor_expr();
		e1 = m_act.on_bitwise_or_expr(e1,e2);
	}
	return e1;
}

expr*
parser::parse_bitwise_and_expr()
{
	expr* e1 = parse_equality_expr();
	while (match_if_bitwise_and()){
		expr* e2 = parse_equality_expr();
		e1 = m_act.on_bitwise_and_expr(e1,e2);
	}
	return e1;
}

expr*
parser::parse_equality_expr()
{
	expr* e1 = parse_relational_expr();
	while (token tok = match_if_equality())
	{
		expr* e2 = parse_relational_expr();
		e1 = m_act.on_equality_expr(tok,e1,e2)
	}
	return e1;
}

expr*
parser::parse_relational_expr()
{
	expr* e1 = parse_shift_expr();
	while (token tok = match_if_relational()){
		expr* e2 = parse_shift_expr();
		e1 = m_act.on_relational_expr(tok, e1, e2);
	}
	return e1;
}

expr*
parser::parse_shift_expr()
{
	expr* e1 = parse_additive_expr();
	while (token tok = match_if_shift()){
		expr* e2 = parse_aditive_expr();
		e1 = m_act.on_shift_expr(tok,e1,e2);
	}
	return e1;
}

expr*
parser::parse_additive_expr()
{
	expr* e1 = parse_multiplicative_expr();
	while (token tok = match_if_additive()){
		expr* e2 = parse_multiplicative_expr();
		e1 = m_act.on_additive_expression(tok, e1, e2);
	}
	return e1;
}

expr*
parser::parse_multiplicative_expr()
{
	expr* e1 = parse_cast_expr();
	while (token tok = match_if_multiplicative()){
		expr* e2 = parse_cast_expr();
		e1 = m_act.on_additive_expr(tok, e1, e2);
	}
	return e1;
}

expr*
parser::parse_cast_expr()
{
	expr* e = parse_unary_expr();
	if(match_if(kw_as)){
		type* t = parse_type();
		return m_act.on_cast_expr(e,t);
	}
	return e;
}

expr*
parser::parse_unary_expr()
{
	token op;
	switch (lookahead()) {
		case token_arithmetic_operator:
			switch(peek().get_arithmetic_operator()){
				case op_plus:
				case op_minus:
				case op_multiply:
					op = accept();
					break;
				default:
					break;
			}
		case token_bitwise_operator:
			switch (peek().get_bitwise_operator()){
			case op_not:
			case op_and:
				op = accept();
				break;
			default:
				break;
			}
		case token_logical_operator:
		if(peek().get_logical_operator() == logical_not)
			op = accept();
		break;
		
		default:
		break;
	}
	
	if(op) {
		expr* e = parse_unary_expr();
		return m_act.on_unary_expr(op,e);
	}
	
	return parse_postfix_expr();
}


expr*
parser::parse_postfix_expr()
{
	expr* e = parse_primary_expr();
	while(true) {
		if (match_if(token_left_paren)){
			expr_list args = parse_argument_list();
			match(token_right_paren);
			e = m_act.on_call_expression(e,args);
		}
		else if (match_if(token_left_bracket)){
			expr_list args = parse_argument_list();
			match(tok_right_bracket);
			e = m_act.on_index_expr(e,args);
		}
		else{
			break;
		}
	}
	return e;
}

expr*
parser::parse_primary_expr()
{
	switch(lookahead())
	{
		case token_bool:
			return m_act.on_bool_literal(accept());
		case token_float:
			return m_act.on_float_literal(accept());
		case token_char:
		case token_str:
			throw std::logic_error("not implemented");
		case token_float:
		case token_bin_int:
		case token_dec_int:
		case token_oct_int:
		case token_hex_int:
			return m_act.on_int_literal(accept());
			
		case token_identifier:
			return m_act.on_id_expr(accept());
			
		case token_left_paren:
		{
			match(token_left_paren);
			expr* e = parse_expr();
			match(token_right_paren);
			return e;
		}
		
		default:
			break;
	}
	throw std::runtime_error("expected primary-expression");
}

expr_list
parser::parse_argument_list()
{
	expr_list args;
	while(true){
		expr* arg = parse_expr();
		if(match_if(token_comma))
			continue;
		if (lookahead() == token_right_paren)
			break;
		if(lookahead() == token_right_brace)
			break;
	}
	return args;
}

//statements

stmt*
parser::parse_statement()
{
	switch(lookahead()){
		case keyword_if:
			return parse_if_statement();
		case keyword_while:
			return parse_while_statement();
		case keyword_break:
			return parse_break_statement();
		case keyword_continue:
			return parse_continue_statement();
		case keyword_return:
			return parse_return_statement();
		case keyword_var:
		case keyword_let:
		case keyword_def:
			return parse_decl_statement();
		case token_left_brace:
			return parse_block_statement();
		default:
			return parse_expr_statement();
	}
}

stmt*
parser::parse_block_statement()
{
	match(token_left_brace);
	m_act.enter_block_scope();
	m_act.start_block();
	
	stmt_list ss;
	if(lookahead() != token_right_brace)
		ss = parse_statement_seq();
	
	m_act.finish_block();
	m_act.leave_scope();
	match(token_right_brace);
	return m_act.on_block_statement(ss);
}

stmt*
parser::parse_if_statement()
{
	assert(lookahead() == keyword_if);
	accept();
	match(token_left_paren);
	expr* e = parse_expr();
	match(token_right_paren);
	stmt* b = parse_statement();
	match(keyword_else);
	st,t* f = pare_statement();
	return m_act.on_if_statement(e,t,f);
}

stmt*
parser::parse_while_statement()
{
	assert(lookahead == keyword_while);
	accept();
	match(token_left_paren);
	expr* e = parse_expr();
	match(token_right_paren);
	stmt* b = parse_statement();
	return m_act.on_while_statement(e,b);
}

stmt*
parser::parse_break_statement()
{
	assert(lookahead() == keyword_break);
	accept();
	match(token_semicolon);
	return m_act.on_break_statement();
}

stmt* 
parser::parse_continue_statement()
{
	assert(lookahead() == keyword_continue);
	accept();
	match(token_semicolon);
	return m_act.on_continue_statement();
}

stmt*
parser::parse_return_statement()
{
	assert(lookahead() == keyword_return);
	accept();
	expr* e = parse_expression();
	match(token_semicolon);
	return m_act.on_return_statement(e);
}

stmt*
parser::parse_decl_statement()
{
	decl* d = parse_local_decl();
	return m_act.on_decl_statement(d);
}

stmt*
parser::parse_expr_statement()
{
	expr* e = parse_expr();
	match(token_semicolon);
	return m_act.on_expr_statement(e);
}

stmt_list
parser::parse_statement_seq
{
	stmt_list ss;
	while(true) {
		stmt* s = parse_statement();
		ss.push_back(s);
		if(lookahead() == token_right_brace)
			break;
	}
	return ss;
}


//declaration

decl*
parser::parse_decl()
{
	switch (lookahead())
	{
		default:
			throw std:runtime_error("expected declaration");
		case keyword_def:
		{
			token_name n =  lookahead(2);
			if (n == token_colon)
				return parse_object_def();
			if (n == token_left_paren)
				return parse_function_def();
			throw std::runtime_error("wrong");
		}
		case keyword_let:
		case keyword_var:
			return parse_object_def();
	}
	return nullptr;
}

decl*
parser::parse_local_decl()
{
	return parse_object_def();
}

decl*
parser::parse_object_def()
{
	switch (lookahead())
	{
		default:
			throw std::runtime_error("expected object-definition");
		case keyword_def:
			return parse_value_def();
		case keyword_let:
			return parse_constant_def();
		case keyword_var:
			return parse_variable_definition();
	}
}

decl*
parser::parse_variable_def()
{
	assert(lookahead() == keyword_var);
	accept();
	token id = match(token_identifier);
	match(token_colon);
	type* t = parse_type();
	
	decl* d = m_act.on_variable_decl(id,t);
	
	match(token_assignment_operator);
	expr* e = parse_expr();
	match(token_semicolon);
}

decl*
parse::parse_constant_def
{
  assert(lookahead() == keyword_let);
  accept();
  token id = match(tok_identifier);
  match(token_colon);
  type* t = parse_type();

  decl* d = m_act.on_constant_decl(id, t);

  match(token_assignment_operator);
  expr* e = parse_expr();
  match(token_semicolon);

  return m_act.on_constant_def(d, e);
}

decl*
parser::parse_value_def()
{
  assert(lookahead() == keyword_def);
  accept();
  token id = match(token_identifier);
  match(token_colon);
  type* t = parse_type();

 
  decl* d = m_act.on_value_declaration(id, t);

  match(tok_assignment_operator);
  expr* e = parse_expression();
  match(tok_semicolon);

  // End of definition.
  return m_act.on_value_definition(d, e);
}



decl*
parser::parse_function_def()
{
  assert(lookahead() == keyword_def);
  accept();
  token id = match(token_identifier);

  match(token_left_paren);
  m_act.enter_parameter_scope();
  decl_list parms;
  if (lookahead() != token_right_paren)
    parms = parse_parameter_clause();
  m_act.leave_scope();
  match(token_right_paren);
  
  match(toke_arrow_operator);
  
  type* t = parse_type();

  // Point of declaration
  decl* d = m_act.on_function_decl(id, parms, t);

  stmt* s = parse_block_statement();
  
  // End of definition
  return m_act.on_function_definition(d, s);
}

decl_list
parser::parse_parameter_clause()
{
  return parse_parameter_list();
}

decl_list
parser::parse_parameter_list()
{
  decl_list parms;
  while (true) {
    assert(lookahead() == keyword_var);
    parms.push_back(parse_parameter());
    if (match_if(tok_comma))
      continue;
    else
      break;
  }
  return parms;
}

decl*
parser::parse_parameter()
{
  token id = match(token_identifier);
  match(token_colon);
  type* t = parse_type();
  return m_act.on_parameter_decl(id, t);
}


decl_list
parser::parse_declaration_seq()
{
	decl_list ds;
	while(peek()){
		decl* d = parse_decl();
		ds.push_back(d);
	}
	return ds;
}

decl*
parser::parse_program()
{
	m_act.enter_global_scope();
	decl_list decls = parse_decl_deq();
	m_act.leave_scope();
	return m_act.on_program(decls);
}