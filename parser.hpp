#pragma once

#include "lexer.hpp"
#include "symbol.hpp"
#include "semantics.hpp"
#include <deque>

class type;
class expr;
class stmt;
class decl;
class prog;

using type_list = std::vector<type*>;
using expr_list = std::vector<expr*>;
using stmt_list = std::vector<stmt*>;
using decl_list = std::vector<decl*>;

class parser
{
public:
	parser(symbol_table& syms, const file& f);

	type* parse_type();
	type* parse_basic_type();
	
	expr* parse_expr();
	expr* parse_assignment_expr();
	expr* parse_conditional_expr();
	expr* parse_logical_or_expr();
	expr* parse_logical_and_expr();
	expr* parse_bitwise_or_expr();
	expr* parse_bitwise_and_expr();
	expr* parse_equality_expr();
	expr* parse_relational_expr();
	expr* parse_shift_expr();
	expr* parse_additive_expr();
	expr* parse_multiplicative_expr();
	expr* parse_cast_expr();
	expr* parse_unary_expr();
	expr* parse_postfix_expr();
	expr* parse_primary_expr();

	stmt* parse_statement();
	stmt* parse_block_statement();
	stmt* parse_if_statement();
	stmt* parse_while_statement();
	stmt* parse_break_statement();
	stmt* parse_continue_statement();
	stmt* parse_return_statement();
	stmt* parse_decl_statement();
	stmt* parse_expression_statement();
	stmt_list parse_statement_seq();



	decl* parse_decl();
	decl* parse_local_decl();
	decl* parse_object_def();
	decl* parse_variable_def();
	decl* parse_constant_def();
	decl* parse_value_def();
	decl* parse_function_def();
	decl* parse_parameter();
	decl_list parse_parameter_list();
	decl_list parse_parameter_clause();
	decl_list parse_decl_seq();

	decl* parse_program();


private:
	token_name lookahead();
	token_name lookahead(int n);
	token match(token_name n);
	token match_if(token_name n);
	token match_if_logical_or();
	token match_if_logical_and();
	token match_if_bitwise_or();
	token match_if_bitwise_xor();
	token match_if_bitwise_and();
	token match_if_equality();
	token match_if_relational();
	token match_if_shift();
	token_match_if_additive();
	token match_if_multiplicative();
	
	token accept();
	token peek();
	void fetch();
	
private:
	lexer m_lex;
	
	std::deque<token> m_tok;
	
	semantics m_act;
};

inline
parser::parser(symbol_table& syms, const file& f)
	: m_lex(syms, f), m_tok()
{
	fetch();
}