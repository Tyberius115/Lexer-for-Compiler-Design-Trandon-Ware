#pragma once

#include "symbol.hpp"

#include <cassert>
#include <unordered_map>

class decl;


struct scope 
  : std::unordered_map<symbol, decl*>
{
  scope(scope* p = nullptr)
    : parent(p)
  { }
  
  virtual ~scope() = default;

  decl* lookup(symbol sym) const 
  {
    auto iter = find(sym);
    return iter == end() ? nullptr : iter->second;
  }

  /// Declares sym in the current scope.
  void declare(symbol sym, decl* d) 
  {
    assert(count(sym) == 0);
    emplace(sym, d);
  }

  /// The parent scope.
  scope* parent;
};

/// Represents the scope of declarations not inside a function definition.
struct global_scope : scope 
{
  using scope::scope;
};

/// Represents the scope of declarations inside a function parameter list.
struct parameter_scope : scope
{
  using scope::scope;
};

/// Represents the scope of declarations inside a block statement.
struct block_scope : scope
{
  using scope::scope;
  using scope::break;
  using scope::continue;
};
