#pragma once

#include <string>
#include <unordered_set>

//represents a unique symbol in the language

using symbol = const std::string*;

//ensures all symbols with the same spelling are unique
class symbol_table
{
public:
	symbol get(const char* str);
	symbol get(const std::string& str);
	
private:
	std::unordered_set<std::string> m_syms;
};

//returns the symbol corresponding to the string
inline symbol
symbol_table::get(const char* str)
{
	return &*m_syms.insert(str).first
}

inline symbol
symbol_table::get(const std::string& str)
{
	return &*m_syms.insert(str).first;
}