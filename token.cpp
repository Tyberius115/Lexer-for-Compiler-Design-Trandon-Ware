#include "token.hpp"

#include <cassert>
#include <iostream>
#include <iomanip>

const char*
to_string(token_name n)
{
	switch (n) 
	{
		case token_eof:
			return "eof";
		
		case token_left_brace:
			return "left-brace";
		case token_right_brace:
			return "right-brace";
		case token_left_paren:
			return "left-paren";
		case token_right_paren:
			return "right-paren";
		case token_left_bracket:
			return "left-bracket";
		case token_right_bracket:
			return "right-bracket";
		case token_comma:
			return "comma";
		case token_semicolon:
			return "semicolon";
		case token_colon:
			return "colon";
		
		case token_relational_operator:
			return "relational-operator";
		case token_arithmetic_operator:
			return "arithmetic-operator";
		case token_bitwise_operator:
			return "bitwise-operator";
		case token_logical_operator:
			return "logical-operator";
		case token_conditional_operator:
			return "conditional-operator";
		case token_assignment_operator:
			return "assignment-operator";
		case keyword_and:
			return "and";
		case keyword_as:
			return "as";
		case keyword_bool:
			return "bool";
		case keyword_break:
			return "break";
		case keyword_char:
			return "char";	
		case keyword_continue:
			return "continue";
		case keyword_def:
			return "def";
		case keyword_else:
			return "else";
		case keyword_false:
			return "false";
		case keyword_float:
			return "float";	
		case keyword_if:
			return "if";
		case keyword_int:
			return "int";	
		case keyword_let:
			return "let";
		case keyword_not:
			return "not";
		case keyword_or:
			return "or";
		case keyword_return:
			return "return";
		case keyword_true:
			return "true";	
		case keyword_var:
			return "var";
		case keyword_void:
			return "void";
		case keyword_while:
			return "while";	
			
		case token_identifier:
			return "identifier";
		case token_bin_int:
			return "binary-int"
		case token_dec_int:
			return "decimal-int";
		case token_oct_int:
			return "octal-int";
		case token_hex_int:
			return "hexadecimal-int";
		case token_bool:
			return "boolean";
		case token_float:
			return "floating-point";
		case token_char:
			return "character";
		case token_str:
			return "string";
		case token_type_specifier:
			return "type-specifier";
	}
}


//The different relational ops
const char*
to_string(relational_ops op)
{
	switch(op)
	{
		case op_eq:
			return "eq";
		case op_ne:
			return "ne";
		case op_lt:
			return "lt";
		case op_gt:
			return "gt";
		case op_loe:
			return "loe";
		case op_goe:
			return "goe";
	}
};

const char*
to_string(arithmetic_ops op)
{
	switch(op)
	{
		case op_plus:
			return "plus";
		case op_minus:
			return "minus";
		case op_multiply:
			return "multiply";
		case op_divide:
			return "divide";
		case op_remainder:
			return "remainder";
	}
};

const char*
to_string(bitwise_ops op)
{
	switch(op)
	{
		case op_and:
			return "and";
		case op_or:
			return "or";
		case op_not:
			return "not";
		case op_shift_right:
			return "shift-right";
		case op_shift_left:
			return "shift-left";
		case op_xor:
			return "xor";
	}
};

const char*
to_string(logical_ops logical)
{
	switch(logical)
	{
		case logical_and:
			return "and";
		case logical_or:
			return "or";
		case logical_not:
			return "not";
	}
};

const char*
to_string(type_spec type_s)
{
	switch (type_s)
	{
		case type_s_char:
			return "char";
		case type_s_bool:
			return "bool";
		case type_s_int:
			return "int";
		case type_s_float:
			return "float";
	}
};

token::token()
	: m_name(tok_eof)
	{}
	
token::token(token_name n, token_attr a, location locn)
	: m_name(n), m_attr(a), m_locn(locn)
	{}
	
static bool
has attribute(token_name n)
{
	switch (n)
	{
		default:
			return false;
			
		case token_relational_operator:
		case token_arithmetic_operator:
		case token_bitwise_operator:
		case token_logical_operator:
		case token_identifier:
		case token_bin_int:
		case token_dec_int:
		case token_oct_int:
		case token_hex_int:
		case token_bool:
		case token_float:
		case token_char:
		case token_str:
		case token_type_specifier:
			return true;
	}
}

token::token(token_name n, location locn)
	: m_name(n), m_locn
{
	assert(!has_attribute(n));
}

token::token(symbol sym, location locn)
	: m_name(token_identifier), m_attr(sym), m_locn(locn)
{}	

token::token(relational_ops op, location locn)
	: m_name(token_relational_operator), m_attr(op), m_locn(locn)
{}

token::token(arithmetic_ops op, location locn)
	: m_name(token_arithmetic_operator), m_attr(op), m_locn(locn)
{}

token::token(bitwise_ops op, location locn)
	: m_name(token_bitwise_operator), m_attr(op), m_locn(locn)
{}

token::token(logical_ops logical, location locn)
	: m_name(token_logical_operator), m_attr(logical), m_locn(locn)
{}

token::token(long long val, location locn)
	: token(token_dec_int, dec, val, locn)
{}

static token_name
get_token_name(radix rad)
{
	switch(rad)
	{
		case bin:
			return token_bin_int;
		case dec:
			return token_dec_int;
		case oct:
			return token_oct_int;
		case hex:
			return token_hex_int;
	}
}

//Constructs an integer token with a given base
token::token(radix rad, long long val, location locn)
	:token(get_token_name(rad), rad, val, locn)
{}

static bool
check_radix(token_name n, radix rad)
{
	switch (n)
	{
		case token_bin_int:
			return rad == bin;
		case token_dec_int:
			return rad == dec;
		case token_oct_int:
			return rad == oct;
		case token_hex_int:
			return rad == hex;
		default:
			throw std::logic_error("invalid token name");
	}
}

token::token(token_name n, radix rad, long long val, location locn)
	: m_name(n), m_attr(int_attr{rad, val}), m_locn(locn)
{
	assert(check_radix(n, rad));
}

token::token(double val, location locn)
	: m_name(token_float), m_attr(val), m_locn(locn)
{}

token::token(bool tf, location locn)
	: m_name(token_bool), m_attr(tf), m_locn(locn)
{}

token::token(char c, location locn)
	: m_name(token_char), m_attr(c), m_locn(locn)
{}

token::token(string_attr s, location locn)
	: m_name(token_str), m_attr(s), m_locn(locn)
{}

token::token(type_spec ts, location locn)
	: m_name(token_type_specifier), m_attr(ts), m_locn(locn)
{}

//io-manipulator
static std::string
escape(char c)
{
	switch (c)
	{
		default: return {c};
		case '\'': return "\\\'"; //single quote
		case '\"': return "\\\""; //inverted commas
		case '\?': return "\\?"; //question mark
		case '\\': return "\\\\"; //backslash
		case '\a': return "\\a"; //alert
		case '\b': return "\\b"; //backspace
		case '\f': return "\\f"; //form feed
		case '\n': return "\\n"; //new line
		case '\r': return "\\r"; //return to start of line
		case '\t': return "\\t"; //tab
		case '\v': return "\\v"; //vertical tab
	}
}

static std::string
escape(const std::string& s)
{
	std::string ret;
	for (char c : s)
		ret += escape(c);
	return ret;
}

std::ostream&
operator<<(std::ostream& os, token token)
{
	os << '<';
	
	if(token.get_location())
	{
		os << token.get_location() << ':';
	}
	
	os << to_string(token.get_name());
	switch(token.get_name())
	{
		default:
			break;
		
		case token_relational_operator:
			os << ':' << to_string(token.get_relational_operator());
			break;
			
		case token_arithmetic_operator:
			os << ':' << to_string(token.get_arithmetic_operator());
			break;
			
		case token_bitwise_operator:
			os << ':' << to_string(token.get_bitwise_operator());
			break;
			
		case token_logical_operator:
			os << ':' << to_string(token.get_logical_operator());
			break;
			
		case token_identifier:
			os << ':' << to_string(token.get_identifier());
			break;
			
		case token_bin_int:
		case token_dec_int:
		case token_oct_int:
		case token_hex_int:
		
			os << ':' << std::setbase(token.get_radix()) << token.get_integer();
			break;
		
		case token_boolean:
			os << ':' << token.get_boolean();
			break;
			
		case token_float:
			os << ':' << token_get_float();
			break;
			
		case token_char:
			os << ':' << escape(token.get_char());
			break;
		case token_str:
			os << ':' << escape(token.get_str());
			break;
			
		case token_type_specifier:
			os << ':' << to_string(token.get_type_specifier());
			break;
	}
	os << '>';
	return os;
}