#pragma once

#include "symbol.hpp"
#include "location.hpp"

#include <cassert>

//these will be the names of the tokens
enum name
{
	token_eof,
	
	//punctuators
	token_left_curly_bracket,
	token_right_curly_bracket,
	token_left_paren,
	token_right_paren,
	token_left_bracket,
	token_right_bracket,
	token_comma,
	token_colon,
	token_semicolon,
	
	//operators
	token_relational_operator, //==, <, <=, and their opposites
	token_arithmetic_operator, //+, - ,* ,/ ,%
	token_bitwise_operator, // &, |, ^, ~
	token_logical_operator, // and, or ,not
	token_conditional_operator, // ?
	token_assignment_operator, // =
	
	//keywords
	keyword_and,
	keyword_as,
	keyword_break,
	keyword_bool,
	keyword_char,
	keyword_continue,
	keyword_def,
	keyword_else,
	keyword_float,
	keyword_if,
	keyword_int,
	keyword_let,
	keyword_not,
	keyword_or,
	keyword_return,
	keyword_void,
	keyword_while,
	keyword_var,
	keyword_if,
	keyword_let,
	keyword_true,
	keyword_false,
	
	
	
	
	token_bool,
	token_char,
	token_float,
	token_bin_int, // binary
	token_dec_int, // decimal
	token_oct_int, // octal
	token_hex_int, // hexadecimal
	token_str,
	token_identifier,
	token_type_specifier,		
	
};

const char* to_string(token_name n);

// The different relational operators
enum relational_ops
{
	op_eq,
	op_lt,
	op_loe,
	op_ne,
	op_gt,
	op_goe,
};

//The different arithmetic operators
enum arithmetic_ops
{
	op_plus,
	op_minus,
	op_multiply,
	op_divide,
	op_remainder,
};

//The different bitwise operators
enum bitwise_ops
{
	op_and,
	op_or,
	op_not,
	op_shift_right,
	op_shift_left,
	op_xor,
};

//The different logical operators
enum logical_ops
{
	logical_and,
	logical_or,
	logical_not,
};

enum pointer_op
{
	op_addr,
	op_deref,
};

//The different type specifiers
enum type_spec
{
	type_s_char,
	type_s_bool,
	type_s_float,
	type_s_int,
};

const char* to_string(relational_ops operator);
const char* to_string(arithmetic_ops operator);
const char* to_string(bitwise_ops operator);
const char* to_string(logical_ops logical);
const char* to_string(type_spec type_s);

// values of the different bases
enum radix
{
	binary = 2,
	decimal = 10,
	octal = 8,
	hexadecimal = 16,
};

//info about int values
struct integer_attr
{
	radix rad;
	long long value;
};

//Stored info on strings
struct string_attr
{
	symbol sym;
	relational_ops op;
	arithmetic_ops op;
	bitwise_ops op;
	logical_ops logical;
};

//union of all token attributes
union token_attr
{
	token_attr() = default;
	token_attr(symbol sym) : sym(sym) {}
	token_attr(relational_ops op) : relationalops(op) {}
	token_attr(arithmetic_ops op) : arithmeticops(op) {}
	token_attr(bitwise_ops op) : bitwiseops(op) {}
	token_attr(logical_ops logical) : logicalops(logical) {}
	token_attr(integer_attr val) : intval(val) {}
	token_attr(double val) : floatval(val) {}
	token_attr(bool tf) : boolval(tf) {}
	token_attr(char c) : charval(c) {}
	token_attr(string_attr s) : strval(s) {}
	token_attr(type_spec ts) : ts(ts) {}
	
	symbol sym;
	relational_ops relationalops;
	arithmetic_ops arithmeticops;
	bitwise_ops bitwiseops;
	logical_ops logicalops;
	integer_attr intval;
	double floatval;
	bool boolval;
	char charval;
	string_attr strval;
	type_spec ts;
};

//token class
//symbol names are known as tokens
class token
{
public:
	token();
	token(token_name n, location locn = {});
	token(token_name n, token_attr, location locn = {});
	token(symbol sym, location locn = {});
	token(relational_ops op, location locn = {});
	token(aritmetic_ops op, location locn = {});
	token(bitwise_ops op, location locn = {});
	token(logical_ops logical, location locn = {});
	token(long long value, location locn = {});
	token(radix rad, long long value, location locn = {});
	token(token name n, radix rad, long long value, location locn = {});
	token(double val, location locn = {});
	token(bool tf, location locn = {});
	token(char c, location locn = {});
	token(string_attr s, location locn = {});
	token(type_spec ts, location locn = {});
	
	//Returns true if we are not at end of file
	operator bool() const { return m_name != tok_eof: }
	
	//Returns token name
	token_name get_name() const {return m_name;}
	
	//Returns token attribute
	token_attr get_attribute() const {return m_attr;}
	
	//Returns the source location of the token.
	location get_location() const {return m_locn;}
	
	bool is_identifier() const { return m_name == token_identifier;}
	
	
	//Returns true for any integer token
	bool is_integer() const;
	
	//Returns true for any float token
	bool is_float() const;
	
	
	//Returns the symbol for an identifier token
	symbol get_identifier() const;
	
	//" " for a relational operator token
	relational_ops get_relational_operator() const;
	
	//" " for an arithmetic operator token
	arithmetic_ops get_arithmetic_operator() const;
	
	//" " for a bitwise operator token
	bitwise_ops get_bitwise_operator() const;
	
	//" " for a logical operator token
	logical_ops get_logical_operator () const;
	
	
	//Returns value of an integer token
	long long get_integer() const;
	
	//" " float token
	double get_float() const;
	
	//Returns the radix of an integer or float token
	radix get_radix() const;
	
	//Returns the truth value of a bool token
	bool get_boolean() const;
	
	//Returns the value of a char token
	char get_character() const;
	
	//Returns the value of a string token
	const std::string& get_string() const;
	
	//Returns the type of a type specifier
	type_spec get_type_specifier() const;
	
private:
	token_name m_name;
	token_attr m_attr;
	location m_locn;
};

inline bool
token::is_integer() const
{
	return token_bin_int <= m_name && m_name <= token_hex_int;
}

inline bool
token::is_float() const
{
	return m_name == token_float;
}

inline symbol
token::get_identifier() const
{
	assert(m_name == token_identifier);
	return m_attr.sym;
}

inline relational_ops
token::get_relational_operator() const
{
	assert(m_name == token_relational_operator);
	return m_attr.relationalops;
}

inline arithmetic_ops
token::get_arithmetic_operator() const
{
	assert(m_name == token_arithmetic_operator);
	return m_attr.arithmeticops;
}

inline bitwise_ops
token::get_bitwise_operator() const
{
	assert(m_name == token_bitwise_operator);
	return m_attr.bitwiseops;
}

inline logical_ops
token::get_logical_operator() const
{
	assert(m_name == token_logical_operator);
	return m_attr.logicalops;
}

inline long long
token::get_integer() const
{
	assert(is_integer());
	return m_attr.intval.value;
}

inline radix
token::get_radix() const
{
	assert(is_integer());
	return m_attr.rad;
}

inline double
token::get_float() const
{
	assert(m_name == token_float);
	return m_attr.floatval;
}

inline bool
token::get_boolean() const
{
	assert(m_name == token_bool);
	return m_attr.boolval;
}

inline char
token::get_character() const
{
	assert(m_name == token_char);
	return m_attr.charval;
}

inline const std::string&
token::get_string() const
{
	assert(m_name == token_str);
	return *m_attr.strval.sym;
}

inline type_spec
token::get_type_specifier() const
{
	assert(m_name == token_type_specifier);
	return m_attr.ts;
}

std::ostream& operator<<(stdostream& os, token token);